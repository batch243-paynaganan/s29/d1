// [section]comparison query operators
/*
	$gt/$gte operator
	it allows us to find document that have field number value greater than or equal to a specified value.
	syntax:
	db.collectionName.find({field: {$gt: value}});
	db.collectionName.find({field: {$gte: value}});
*/
db.users.find({age: {$gt:50}}).pretty();

db.users.find({age: {$gte:21}}).pretty();

// $lt/$lte operator
/*
	syntax:
	db.collectionName.find({field: {$lt: value}});
	db.collectionName.find({field: {$lte: value}});
*/
	db.users.find({age: {$lt: 50}});
	db.users.find({age: {$lte: 76}});

// $ne operator

	/*
		syntax:
		db.collectionName.find({field: {$ne:value}})
	*/

db.users.find({age: {$ne: 68}});


// $in operator
/*
	syntax:
		db.collectionName.find({field: {$in: [valueA, valueB]}});
*/

db.users.find({lastName: {$in: [ "Doe", "Hawking"]}});

db.users.find({courses: {$in: ["HTML", "React"]}});

db.users.find({courses: {$in: ["CSS", "JavaScript"]}});


// [section] logical query operator
/*
	$or operator:
	db.collectionName.find({$or: [{fieldA: valueA},{fieldB: valueB}]});
*/

db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

db.users.find({$or: [{firstName: "Neil"}, {age: {$gt:30}}]});
// $and operator
/*
	syntax:
	db.collectionName.find({$and: [{fieldA: valueA},{fieldB: valueB}]})
*/

db.users.find({$and: [{firstName: "Neil"}, {age: {$gt:30}}]});

db.users.find({$and: [{firstName: "Neil"}, {age: 25}]});

// [Section] Field projection
// inclusion
/*
	syntax:
	db.collectionName.find({criteria},{fields: 1/true});
*/

db.users.find({firstName: "Jane"},{
	firstName: true,
	lastName: 1,
	age: 1
});

db.users.find({firstName: "Jane"},{
	firstName: 1,
	lastName: 1,
	age: 1
});

db.users.find({$and: [{firstName: "Neil"}, {age: {$gt:30}}]},{
	firstName: 1,
	lastName: true,
	contact: true
});

// Exclusion
/*
	Syntax:
	db.collectionName.find({criteria},{fields: 0/false});
*/

db.users.find({firstName: "Jane"},{
	contact: 0,
	department: false
})

db.users.find({$and: [{firstName: "Neil"}, {age: {$gt:30}}]},{
	age: false,
	courses: 0
});

// Return specific fields in embedded documents

db.users.find({firstName: "Jane"},{
	"contact.phone" : 1,
})

db.users.find({firstName: "Jane"},{
	"contact.phone" : 0,
})


// [Section] Evaluation query operators
/*
	db.users
*/

db.users.find({firstName: {$regex: "N"}});


// crud operations

db.users.updateOne({age: {$lte: 17}}, {
	$set: {
		firstName: "Chris",
		lastName: "Mortel"
	}
})

db.users.deleteOne({$and:[{firstName: "Chris"}, {lastName: "Mortel"}]})